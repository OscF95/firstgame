using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField] public Transform objectToFollow;
    public Vector3 offset;

    void Start()
    {
        offset = transform.position;
    }

    void Update()
    {
        
        transform.position = objectToFollow.position + offset;
    }
}