using UnityEngine;

public class vikingMovements : MonoBehaviour
{
    private Animator _animator;
    private float _horizontalInput;
    private bool _back;
    private bool attack;
    private float _speed = 3;
    private int _agleToGoBack = 180;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        _horizontalInput = Input.GetAxis("Horizontal");
        managePlayerState();
    }

    void managePlayerState()
    {
        if (_horizontalInput != 0 && !checkIfAttack())
        {
            movePlayer();
        }
        else
        {
            stopPlayer();
            checkIfAttack();
        }
    }

    void movePlayer()
    {
        if (Input.GetKey(KeyCode.A) && !_back)
        {
            transform.Rotate(Vector3.down * _agleToGoBack);
            _back = true;
        }
        else if (Input.GetKey(KeyCode.D) && _back)
        {
            transform.Rotate(Vector3.down * _agleToGoBack);
            _back = false;
        }

        if (_back)
        {
            _horizontalInput = -_horizontalInput;
        }

        transform.Translate(Vector3.forward * _horizontalInput * _speed * Time.deltaTime);
        _animator.SetFloat("movements", _horizontalInput);
    }

    void stopPlayer()
    {
        _animator.SetFloat("movements", 0f);
    }

    bool checkIfAttack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            _animator.SetTrigger("attack");
            attack = true;
        }
        if (attack)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                attack = false;
            }
        }
        return attack;
    }
}
