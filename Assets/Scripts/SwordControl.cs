using UnityEngine;
using UnityEngine.SceneManagement;

public class SwordControl : MonoBehaviour
{
    AudioSource audioSource;
    Animator animator;
    AnimatorStateInfo stateInfo;
    private bool _soundPlayed;


    void Update()
    {
        CheckAnimationAndReproduceSound(); 
    }

    void OnCollisionEnter(Collision collision)
    {
        animator = GetComponentInParent<Animator>();
        if (animator != null)
        {
            stateInfo = animator.GetCurrentAnimatorStateInfo(0);

            if (stateInfo.IsName("Attack"))
            {
                Destroy(collision.gameObject);
                GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
                if (balls.Length == 1)
                {
                    SceneManager.LoadScene("Level2");
                }
            }

        }
    }

    void CheckAnimationAndReproduceSound() 
    {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponentInParent<Animator>();
        stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.IsName("Attack"))
        {
            
            if (!_soundPlayed)
            {
                audioSource.Play();
                _soundPlayed = true;
            }
        }
        else
        {
            _soundPlayed = false;
        }
    }
}
